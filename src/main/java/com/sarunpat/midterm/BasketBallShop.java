package com.sarunpat.midterm;

public class BasketBallShop {
    private int s;
    private String type;
    private String name;
    private double price;
    private String unit;

    public BasketBallShop(int s ,String type,String name, double price , String unit){
        this.s = s;
        this.type = type;
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public void printMenu(){
        System.out.println(s + ". " +type + " : " + name + " ราคา " + price + "บาท");
    }

    public void printChoice(){
        System.out.println("สินค้าที่เลือก : " + name);
    }

    public double Total(int quantity){
        double total = price*quantity;
        return total;
    }

    public void printTotal(int quantity){
        System.out.println(name + " จำนวน " + quantity +" " + unit);
        System.out.println("ราคา " + Total(quantity) + " บาท");

    }
}

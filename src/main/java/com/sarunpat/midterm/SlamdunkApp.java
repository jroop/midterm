package com.sarunpat.midterm;

import java.util.Scanner;


/**
 * Hello world!
 *
 */
public class SlamdunkApp {
    static Slamdunk Sakuragi = new Slamdunk("Sakuragi", 85);
    static Slamdunk Rukawa = new Slamdunk("Rukawa",94);
    static Slamdunk Sendoh = new Slamdunk("Sendoh",96);

    
    public static String input() {
        System.out.print("เลือกดูค่าความแม่นยำตั้งต้นในการชู้ตของตัวละครหรือ E เพื่อ ออกจากโปรแกรม : ");
        Scanner sc = new Scanner(System.in);
        return sc.next();
        
    }

    public static int inputShootingRange() {
        String range;
        int DR;
        System.out.print("กรอกระยะการชู้ตเพื่อดูค่าความแม่นยำในระยะที่ต้องการ : ");
        Scanner sc = new Scanner(System.in);
        range = sc.next();
        DR = Integer.parseInt(range);
        return DR;
        
    }

    public static void Action(String choose){
        double DR;
        switch(choose){
            case "s":
                Sakuragi.print();
                DR = inputShootingRange();
                Sakuragi.printshoot(DR);
                break;
            case "r":
                Rukawa.print();
                DR = inputShootingRange();
                Rukawa.printshoot(DR);
                break;
            case "z":
                Sendoh.print();
                DR = inputShootingRange();
                Sendoh.printshoot(DR);
                break;
            case "E":
                System.exit(0);
                break;
        }

    }

    public static void main(String[] args){
        while(true){
            String choose = input();
            Action(choose);

            
        }
        
    }
}

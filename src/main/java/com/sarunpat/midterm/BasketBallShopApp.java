package com.sarunpat.midterm;

import java.util.Scanner;

public class BasketBallShopApp {
    static BasketBallShop product1 = new BasketBallShop(1, "BasketBall Shoes(รองเท้าบาสเกตบอล)", "Nike PG5(GW Vers.)",
            4200, "คู่");
    static BasketBallShop product2 = new BasketBallShop(2, "BasketBall(บาสเกตบอล)", "Spalding NBA Gold Series", 1590,
            "ลูก");
    static BasketBallShop product3 = new BasketBallShop(3, "BasketBall Jersey(เสื้อบาสเกตบอล)",
            "GlobalHooper Winter(Black)", 1639, "ตัว");

    public static void Greeting() {
        System.out.println("Welcome to BBShop");
    }

    public static String inputChoice() {
        System.out.print("เลือกสินค้าที่ต้องการซื้อ หรือกด q เพื่อออกจากโปรแกรม : ");
        Scanner sc = new Scanner(System.in);
        return sc.next();
    }

    public static int inputQ() {
        String strQuantity;
        int quantity;
        System.out.print("ใส่จำนวนสินค้าที่ต้องการ : ");
        Scanner sc = new Scanner(System.in);
        strQuantity = sc.next();
        quantity = Integer.parseInt(strQuantity);
        return quantity;
    }

    public static void Order(String choice) {
        int quantity;
        switch (choice) {
            case "1":
                product1.printChoice();
                quantity = inputQ();
                product1.printTotal(quantity);
                break;
            case "2":
                product2.printChoice();
                quantity = inputQ();
                product2.printTotal(quantity);
                break;
            case "3":
                product3.printChoice();
                quantity = inputQ();
                product3.printTotal(quantity);
                break;
            case "q":
                System.out.println();
                System.out.println("ขอบคุณที่อุดหนุน");
                System.exit(0);
                break;
        }
    }

    

    public static void main(String[] args) {
        Greeting();
        product1.printMenu();
        product2.printMenu();
        product3.printMenu();
        System.out.println();
        while (true) {
            String choice = inputChoice();
            Order(choice);
        }
    }

}

package com.sarunpat.midterm;
import java.lang.Math;

public class Slamdunk {
    private int FGP;//อัตราการชู้ตลงแรกเริ่มของตัวละคร
    private String name; //ชื่อตัวละคร
    public Slamdunk(String name, int FGP) {
        this.name = name;
        this.FGP = FGP;
    }

    public void printshoot(double DR){
        double rate = (FGP - (Math.sqrt(DR*(Math.sqrt((FGP/2))+DR))));
        if (rate<=0){
            System.out.println("ไกลเกินไป "+name+" ชู้ตไม่ลงอย่างแน่นอน");
            System.out.println();
        }

        else if(rate<=15){
            System.out.println("ระยะชู้ตไกลมากหวังเสี่ยงดวงกันเลยที่เดียว");
            System.out.println("ค่าความแม่นยำในการชู้ตของ " +name+"ในระยะ "+DR+" = "+ rate + " %");
            System.out.println();
        }

        else if(rate<=30){
            System.out.println("ถ้าได้แต้มก็ถือว่าโชคดี");
            System.out.println("ค่าความแม่นยำในการชู้ตของ " +name+"ในระยะ "+DR+" = "+ rate + " %");
            System.out.println();
        }
        
        else if(rate<50){
            System.out.println(name + " เล่นลูก3คะแนน!!!");
            System.out.println("ค่าความแม่นยำในการชู้ตของ " +name+"ในระยะ "+DR+" = "+ rate + " %");
            System.out.println();
        }

        else if(rate<=60){
            System.out.println("ระยะประมาณนี้กำลังดี");
            System.out.println("ค่าความแม่นยำในการชู้ตของ " +name+"ในระยะ "+DR+" = "+ rate + " %");
            System.out.println();
        }

        else if(rate<=80){
            System.out.println("ฟอร์มชู้ตสวยมาก "+ name+" !!!");
            System.out.println("ค่าความแม่นยำในการชู้ตของ " +name+"ในระยะ "+DR+" = "+ rate + " %");
            System.out.println();
        }

        else{
            System.out.println("อัจฉริยะ" + name);
            System.out.println("ค่าความแม่นยำในการชู้ตของ " +name+"ในระยะ "+DR+" = "+ rate + " %");
            System.out.println();}
    }



    public void print(){
        System.out.println("ค่าความแม่นยำตั้งต้นในการชู้ตของ " +name+" = "+ FGP + "%");
        System.out.println();
    }


}
